# about.po translation to Spanish
# Copyright (C) 2009-2019 Free Software Foundation, Inc.
# This file is distributed under the same license as the release-notes.
#
# Copyright (C) Translators and reviewers (see below) of the Debian Spanish
# translation team
# - Translators:
# Ricardo Cárdenes Medina
# Juan Manuel García Molina
# Javier Fernández-Sanguino Peña
# David Martínez Moreno
# Francisco Javier Cuadrado <fcocuadrado@gmail.com>, 2009
# - Reviewers:
# Martín Ferrari, 2007
# Javier <jac@inventati.org>, 2019
# Changes:
# - Updated for Lenny
# Javier Fernández-Sanguino Peña <jfs@debian.org>, 2009
# - Updated for Squeeze
# Javier Fernández-Sanguino Peña <jfs@debian.org>, 2010-2011
# - Updated for Wheezy
# Javier Fernández-Sanguino Peña <jfs@debian.org>, 2013
# - Updated for Jessie
# Javier Fernández-Sanguino Peña <jfs@debian.org>, 2015
# - Updated for Stretch
# Javier Fernández-Sanguino Peña <jfs@debian.org>, 2017
# - Updated for Buster
# Javier Fernández-Sanguino Peña <jfs@debian.org>, 2019
# - Updated for Bullseye
# Javier Fernández-Sanguino Peña <jfs@debian.org>, 2021
# Traductores, si no conocen el formato PO, merece la pena leer la
# documentación de gettext, especialmente las secciones dedicadas a este
# formato, por ejemplo ejecutando:
# info -n '(gettext)PO Files'
# info -n '(gettext)Header Entry'
# Equipo de traducción al español, por favor lean antes de traducir
# los siguientes documentos:
# - El proyecto de traducción de Debian al español
# http://www.debian.org/intl/spanish/
# especialmente las notas de traducción en
# http://www.debian.org/intl/spanish/notas
# - La guía de traducción de po's de debconf:
# /usr/share/doc/po-debconf/README-trans
# o http://www.debian.org/intl/l10n/po-debconf/README-trans
msgid ""
msgstr ""
"Project-Id-Version: release-notes 5.0\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-10-06 00:00+0200\n"
"PO-Revision-Date: 2021-08-13 08:41+0200\n"
"Last-Translator: Javier Fernández-Sanguino <jfs@debian.org>\n"
"Language: es\n"
"Language-Team: Debian l10n Spanish <debian-l10n-spanish@lists.debian.org>"
"\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../about.rst:4
msgid "Introduction"
msgstr "Introducción"

#: ../about.rst:6
msgid ""
"This document informs users of the Debian distribution about major "
"changes in version |RELEASE| (codenamed |RELEASENAME|)."
msgstr ""
"Este documento informa a los usuarios de la distribución Debian sobre "
"los cambios más importantes de la versión |RELEASE| (nombre en clave "
"«|RELEASENAME|»)."

#: ../about.rst:9
msgid ""
"The release notes provide information on how to upgrade safely from "
"release |OLDRELEASE| (codenamed |OLDRELEASENAME|) to the current release "
"and inform users of known potential issues they could encounter in that "
"process."
msgstr ""
"Las notas de publicación proporcionan la información sobre cómo "
"actualizar de una forma segura desde la versión |OLDRELEASE| (nombre en "
"clave |OLDRELEASENAME|) a la versión actual e informan a los usuarios "
"sobre los problemas conocidos que podrían encontrarse durante este "
"proceso."

#: ../about.rst:14
msgid ""
"You can get the most recent version of this document from "
"|URL-R-N-STABLE|."
msgstr ""
"Puede obtener la versión más reciente de este documento en |URL-R-N-STABLE|."

#: ../about.rst:19
msgid ""
"Note that it is impossible to list every known issue and that therefore a"
" selection has been made based on a combination of the expected "
"prevalence and impact of issues."
msgstr ""
"Tenga en cuenta que es imposible hacer una lista con todos los posibles "
"problemas conocidos y que, por tanto, se ha hecho una selección de los "
"problemas más relevantes basándose en una combinación de la frecuencia "
"con la que pueden aparecer y su impacto en el proceso de actualización."

#: ../about.rst:23
msgid ""
"Please note that we only support and document upgrading from the previous"
" release of Debian (in this case, the upgrade from |OLDRELEASENAME|). If "
"you need to upgrade from older releases, we suggest you read previous "
"editions of the release notes and upgrade to |OLDRELEASENAME| first."
msgstr ""
"Tenga en cuenta que solo se da soporte y se documenta la actualización "
"desde la versión anterior de Debian (en este caso, la actualización desde"
" |OLDRELEASENAME|). Si necesita actualizar su sistema desde una versión"
" más antigua, le sugerimos que primero actualice a la versión "
"|OLDRELEASENAME| consultando las ediciones anteriores de las notas de "
"publicación."

#: ../about.rst:32
msgid "Reporting bugs on this document"
msgstr "Cómo informar de fallos en este documento"

#: ../about.rst:34
msgid ""
"We have attempted to test all the different upgrade steps described in "
"this document and to anticipate all the possible issues our users might "
"encounter."
msgstr ""
"Hemos intentado probar todos los posibles pasos de actualización "
"descritos en este documento y anticipar todos los problemas posibles con "
"los que un usuario podría encontrarse."

#: ../about.rst:38
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or"
" information that is missing) in this documentation, please file a bug in"
" the `bug tracking system <https://bugs.debian.org/>`__ against the "
"**release-notes** package. You might first want to review the `existing "
"bug reports <https://bugs.debian.org/release-notes>`__ in case the issue "
"you've found has already been reported. Feel free to add additional "
"information to existing bug reports if you can contribute content for "
"this document."
msgstr ""
"En cualquier caso, si piensa que ha encontrado una errata en esta "
"documento, mande un informe de error (en inglés) al `sistema de "
"seguimiento de fallos <https://bugs.debian.org/>`__ contra el paquete "
"**release-notes**. Puede que desee revisar primero los `informes de erratas "
"existentes <https://bugs.debian.org/release-notes>`__ para ver si el problema que Vd. ha encontrado ya se ha"
" reportado. Siéntase libre de añadir información adicional a informes de "
"erratas existentes si puede ayudar a mejorar este documento."

#: ../about.rst:47
#, fuzzy
msgid ""
"We appreciate, and encourage, reports providing patches to the document's"
" sources. You will find more information describing how to obtain the "
"sources of this document in `Sources for this document <#sources>`__."
msgstr ""
"Apreciamos y le animamos a que nos envíe informes incluyendo parches a "
"las fuentes del documento. Puede encontrar más información describiendo "
"cómo obtener las fuentes de este documento en `Sources for this document <#sources>`__."

#: ../about.rst:55
msgid "Contributing upgrade reports"
msgstr "Cómo contribuir con informes de actualización"

#: ../about.rst:57
msgid ""
"We welcome any information from users related to upgrades from "
"|OLDRELEASENAME| to |RELEASENAME|. If you are willing to share "
"information please file a bug in the `bug tracking system "
"<https://bugs.debian.org/>`__ against the **upgrade-reports** package "
"with your results. We request that you compress any attachments that are "
"included (using ``gzip``)."
msgstr ""
"Agradecemos cualquier información que los usuarios quieran proporcionar "
"relacionada con las actualizaciones desde la versión |OLDRELEASENAME| a la "
"versión |RELEASENAME|. Si está dispuesto a compartir la información, por "
"favor mande un informe de fallo al `sistema de seguimiento de fallos "
"<https://bugs.debian.org/>`__. Utilice para el informe el paquete "
"**upgrade-reports** y envíenos el "
"resultado de su actualización. Por favor, comprima cualquier archivo adjunto "
"que incluya (utilizando ``gzip``)."

#: ../about.rst:63
msgid ""
"Please include the following information when submitting your upgrade "
"report:"
msgstr ""
"Le agradeceríamos que incluyera la siguiente información cuando envíe su "
"informe de actualización:"

#: ../about.rst:66
msgid ""
"The status of your package database before and after the upgrade: "
"**dpkg**'s status database available at ``/var/lib/dpkg/status`` and "
"**apt**'s package state information, available at "
"``/var/lib/apt/extended_states``. You should have made a backup before "
"the upgrade as described at :ref:`data-backup`, but you can also find "
"backups of ``/var/lib/dpkg/status`` in ``/var/backups``."
msgstr ""
"El estado de su base de datos de paquetes antes y después de la "
"actualización: la base de datos del estado de **dpkg** (disponible en el archivo ``/var/lib/dpkg/"
"status``) y la información del estado de los paquetes de "
"<systemitem role=\"package\">apt</systemitem> (disponible en el archivo "
"``/var/lib/apt/extended_states``). Debería realizar una "
"copia de seguridad de esta información antes de hacer la actualización, tal "
"y como se describe en :ref:`data-backup`, aunque también puede "
"encontrar copias de seguridad de ``/var/lib/dpkg/status`` "
"en el directorio ``/var/backups``."

#: ../about.rst:74
msgid ""
"Session logs created using ``script``, as described in :ref:`record-"
"session`."
msgstr ""
"Los registros de la sesión que haya creado al utilizar "
"``script``, tal y como se describe en :ref:`record-session`."

#: ../about.rst:77
msgid ""
"Your **apt** logs, available at ``/var/log/apt/term.log``, or your "
"``aptitude`` logs, available at ``/var/log/aptitude``."
msgstr ""
"Sus registros de **apt**, disponibles en el archivo ``/var/log/apt/term.log``, o "
"sus registros de **aptitude**, disponibles en el archivo ``/var/log/aptitude``."

#: ../about.rst:82
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug "
"report as the information will be published in a public database."
msgstr ""
"Debería dedicar algún tiempo a revisar y eliminar cualquier información "
"sensible o confidencial de los registros antes de incluirlos dentro de un"
" informe de fallo ya que la información enviada se incluirá en una base "
"de datos pública."

#: ../about.rst:89
msgid "Sources for this document"
msgstr "Fuentes de este documento"

#: ../about.rst:91
#, fuzzy
msgid ""
"The source of this document is in reStructuredText format, using the "
"sphinx converter. The HTML version is generated using *sphinx-build -b "
"html*. The PDF version is generated using *sphinx-build -b latex*. "
"Sources for the Release Notes are available in the Git repository of the "
"*Debian Documentation Project*. You can use the `web interface "
"<https://salsa.debian.org/ddp-team/release-notes/>`__ to access its files"
" individually through the web and see their changes. For more information"
" on how to access Git please consult the `Debian Documentation Project "
"VCS information pages <https://www.debian.org/doc/vcs>`__."
msgstr ""
"Los archivos fuentes de este documento están en formato DocBook "
"XML. La versión HTML se "
"generó utilizando <systemitem role=\"package\">docbook-xsl</systemitem> y "
"<systemitem role=\"package\">xsltproc</systemitem>. La versión PDF se generó "
"utilizando <systemitem role=\"package\">dblatex</systemitem> o <systemitem "
"role=\"package\">xmlroff</systemitem>. Los ficheros fuente de las notas de "
"publicación están disponibles en el repositorio de Git del "
"*Proyecto de Documentación de Debian*. Puede utilizar la `interfaz web "
"<https://salsa.debian.org/ddp-team/release-notes/>`__ para acceder de "
"forma individual a los archivos y consultar los cambios realizados. Consulte "
"las `páginas de información de Git del "
"Proyecto de Documentación de Debian <https://www.debian.org/doc/vcs>`__ "
"para más información sobre cómo acceder al repositorio de fuentes."
