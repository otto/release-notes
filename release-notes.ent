<!--

These variables are read and used by XML files; they are included
separate from the main document since they may need to change.  This
allows builders to override the defaults without editing the main XML
file.

These entries are language independent

Remember, this is XML; the *first* definition of an ENTITY wins.

-->

<!-- proper (long) name to use for Debian -->
<!ENTITY debian "Debian">

<!-- release data -->
<!ENTITY release "12">
<!ENTITY releasename "bookworm">
<!ENTITY Releasename "Bookworm">
<!ENTITY oldrelease "11">
<!ENTITY oldreleasename "bullseye">
<!ENTITY Oldreleasename "Bullseye">
<!ENTITY nextrelease "13">
<!ENTITY nextreleasename "trixie">
<!ENTITY Nextreleasename "Trixie">
<!-- These values can be obtained using the changes-release.pl script -->
<!ENTITY packages-new "11089">
<!ENTITY packages-total "64419">
<!ENTITY packages-updated "43254">
<!ENTITY packages-update-percent "67">
<!ENTITY packages-removed "6296">
<!ENTITY packages-removed-percent "10">

<!-- URLs -->
<!ENTITY url-release-notes "https://www.debian.org/releases/bookworm/releasenotes">
<!ENTITY url-vcs-release-notes "https://salsa.debian.org/ddp-team/release-notes/">
<!-- TODO: The url-installer links should point to the current status (testing/stable) of the release notes, not just directly to stable -->
<!ENTITY url-installer "https://www.debian.org/releases/bookworm/debian-installer/">
<!ENTITY url-installer-errata "https://www.debian.org/devel/debian-installer/errata">
<!ENTITY url-installer-news "https://www.debian.org/devel/debian-installer/News/">
<!ENTITY url-install-manual "https://www.debian.org/releases/bookworm/installmanual">
<!ENTITY url-bts "https://bugs.debian.org/">
<!ENTITY url-bts-rn "https://bugs.debian.org/release-notes">
<!ENTITY url-debian-backports "https://backports.debian.org/">
<!ENTITY url-debian-i18n "https://www.debian.org/international/">
<!ENTITY url-debian-list-archives "https://lists.debian.org/">
<!ENTITY url-debian-mirrors "https://www.debian.org/distrib/ftplist">
<!-- an example mirror w/o trailing slash -->
<!ENTITY url-debian-mirror-eg "http://mirrors.kernel.org">
<!ENTITY url-irc-host "http://www.oftc.net/">
<!ENTITY debian-irc-server "irc.debian.org">
<!ENTITY url-ports "https://www.debian.org/ports/">
<!ENTITY url-wnpp "https://www.debian.org/devel/wnpp/">
<!ENTITY url-ddp "https://www.debian.org/doc/">
<!ENTITY url-debian-projects "https://www.debian.org/devel/#projects">
<!ENTITY url-debian-blends "https://wiki.debian.org/DebianPureBlends">
<!ENTITY url-debian-jr "https://www.debian.org/devel/debian-jr/">
<!ENTITY url-debian-med "https://www.debian.org/devel/debian-med/">
<!ENTITY url-apt-pin-howto "https://www.debian.org/doc/manuals/apt-howto/ch-apt-get">
<!ENTITY url-securing-debian "https://www.debian.org/doc/manuals/securing-debian-howto/">
<!ENTITY url-debian-backports "http://backports.debian.org/">
<!ENTITY url-ddp-vcs-info "https://www.debian.org/doc/vcs">
<!ENTITY url-wiki "https://wiki.debian.org/">
<!ENTITY url-wiki-selinux "&url-wiki;SELinux">
<!ENTITY url-wiki-newinrelease "&url-wiki;NewInBookworm">
<!ENTITY url-ftpmaster "https://ftp-master.debian.org">
<!ENTITY url-man "https://manpages.debian.org/">
<!ENTITY url-cloud "https://cloud.debian.org/">
<!ENTITY url-cloud-team "https://wiki.debian.org/Teams/Cloud">
<!ENTITY url-cloud-wiki "https://wiki.debian.org/Cloud/">
<!ENTITY url-cloud-wiki-imagelifecycle "https://wiki.debian.org/Cloud/ImageLifecycle">
<!ENTITY url-docker-hub "https://hub.docker.com/_/debian">
<!ENTITY url-vagrant-cloud "https://app.vagrantup.com/debian">

<!ENTITY architecture "<phrase arch='amd64'>amd64</phrase><!--
		    --><phrase arch='arm64'>arm64</phrase><!--
		    --><phrase arch='armel'>armel</phrase><!--
		    --><phrase arch='armhf'>armhf</phrase><!--
		    --><phrase arch='i386'>i386</phrase><!--
		    --><phrase arch='mipsel'>mipsel</phrase><!--
		    --><phrase arch='mips64el'>mips64el</phrase><!--
		    --><phrase arch='ppc64el'>ppc64el</phrase><!--
		    --><phrase arch='s390x'>s390x</phrase>">

<!-- proper nouns for architectures -->
<!ENTITY arch-title "<phrase arch='amd64'>64-bit PC</phrase><!--
		  --><phrase arch='arm64'>64-bit ARM</phrase><!--
		  --><phrase arch='armel'>ARM EABI</phrase><!--
		  --><phrase arch='armhf'>ARMv7 (EABI hard-float ABI)</phrase><!--
		  --><phrase arch='i386'>32-bit PC</phrase><!--
		  --><phrase arch='mipsel'>32-bit MIPS (little endian)</phrase><!--
		  --><phrase arch='mips64el'>64-bit MIPS (little endian)</phrase><!--
		  --><phrase arch='ppc64el'>64-bit little-endian PowerPC</phrase><!--
		  --><phrase arch='s390x'>IBM System z</phrase>">
